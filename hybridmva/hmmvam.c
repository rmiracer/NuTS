/*******************************
  Hybrid Minimum Variance method (HMVA)

  Computes the HMVA method described in the following references

  * Knetter et al., JGR 2004
  * Gosling and Phan, ApJL 2013
  * Mistry et al., GRL 2015
  * Mistry et al., JGR 2017

  It is necessary to have the rotation matrix from the MVA
    method implemented in QSAS.

  This code compiles with (the '-O3' flag is not required :)
    $ gcc hmmva.c -o hmmva.exe -O3 -lm

  The Wind magnetic field data in GSE coordinates, used in the G&P paper
    is included as an example
    (file QSAS_Gosling_Phan_ApJL2013_data/QSAS_data_BxByBz.cef).
  The corresponding MVA rotation matrix obtained with QSAS is also included
    (file QSAS_Gosling_Phan_ApJL2013_data/RotNMatrix_MinVar_QSAS.cef)

  Check the output by executing
  $ ./hmmva.exe QSAS_Gosling_Phan_ApJL2013_data/QSAS_data_BxByBz.cef QSAS_Gosling_Phan_ApJL2013_data/RotNMatrix_MinVar_QSAS.cef 81 27 39

  where 81 is the length of the data series, 27 is the upstream position (line number within the file) of the exhaust, and 39 is the downstream position of the exhaust. Compare the resulting data with Fig. 2 of G&P 2013.

*******************************/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double mag(double *vec)
{
  return sqrt(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2]);
}

double dotprod(double *vec1, double *vec2)
{
  return (vec1[0]*vec2[0] + vec1[1]*vec2[1] + vec1[2]*vec2[2]);
}

void crossprod(double *vec1, double *vec2, double *result)
{
  result[0] = vec1[1]*vec2[2] - vec1[2]*vec2[1];
  result[1] = vec1[2]*vec2[0] - vec1[0]*vec2[2];
  result[2] = vec1[0]*vec2[1] - vec1[1]*vec2[0];
}

int main(int argc, char **argv)
{
  int i, j;
  int pos1init, pos1end, pos2init, pos2end, file_length;
  double temp_modB;
  double org_vec1[3], org_vec2[3], mva_vec[3], hmva_vec[3];
  double Ldir[3], Mdir[3], Ndir[3];
  double *org_B,  *hmva_B;
  FILE *fp1, *fp2, *fp3;

  if(argc < 8)
  {
    fprintf(stderr, "Usage: %s <file_orgB> <file_mvavec> <file_length> <position_1> <position_2>\n", argv[0]);
    fprintf(stderr, "     <file_orgB>   Input data with the three components of the magnetic field vector in three column format.\n");
    fprintf(stderr, "     <file_mvavec> The 3x3 rotation matrix obtained by the MVA vector. The last row should correspond to the L direction. This matrix can be obtained from the MVA implementation found in the QSAS software.\n");
    fprintf(stderr, "     <file_length> Number of rows in file_orgB.\n");
    fprintf(stderr, "     <position_1_init> The upstream exhaust boundary position in file_orgB (line number starting from 0).\n");
    fprintf(stderr, "     <position_1_end> The upstream exhaust boundary position in file_orgB (line number starting from 0).\n");
    fprintf(stderr, "     <position_2_init> The downstream exhaust boundary position in file_orgB (line number starting from 0).\n");
    fprintf(stderr, "     <position_2_end> The downstream exhaust boundary position in file_orgB (line number starting from 0).\n");
  }
  else if(!(fp1 = fopen(argv[1], "r")))
    {
      fprintf(stderr, "Error: Unable to open %s\n", argv[1]);
    } 
    else if(!(fp2 = fopen(argv[2], "r")))
    {
      fprintf(stderr, "Error: Unable to open %s\n", argv[2]);
    } 
    else if(!(fp3 = fopen("output_HMVA.dat", "w")))
    {
      // Go delete your illegal copy of Guardians of the Galaxy!
      fprintf(stderr, "Error: Unable to open output file \"output_HMVA.dat\", disk full?\n");
    } 
    else
    {
      file_length     = atoi(argv[3]);
      pos1init        = atoi(argv[4]);
      pos1end         = atoi(argv[5]);
      pos2init        = atoi(argv[6]);
      pos2end         = atoi(argv[7]);

      org_B  = (double *)malloc(sizeof(double)*3*file_length);
      hmva_B = (double *)malloc(sizeof(double)*3*file_length);

      // Read files
      for(i = 0; i < file_length; i++)
      {
        fscanf(fp1, "%lf %lf %lf\n", &(org_B[0 + 3*i]),
                                     &(org_B[1 + 3*i]), &(org_B[2 + 3*i]));
      }

      // Read MVA rotation matrix computed from QSAS
      // The last row of the 3x3 matrix corresponds
      // to the maximum variance direction (L)
      for(i = 0; i < 3; i++)
      {
        fscanf(fp2, "%lf %lf %lf\n", &(mva_vec[0]),
                                     &(mva_vec[1]), &(mva_vec[2]));
      }

      // Obtain magnetic field vectors adjacent to the exhaust boundaries
      // by computing the average over the interval [pos?init, pos?end]
      // 1/4: Initialize averages to zero
      for(i = 0; i < 3; i++)
      {
        org_vec1[i] = 0.;
        org_vec2[i] = 0.;
      }

      // 2/4: Sum over interval 1
      for(i = pos1init; i < pos1end; i++)
      {
        for(j = 0; j < 3; j++)
        {
          org_vec1[j] += org_B[j + 3*i];
        }
      }

      // 3/4: Sum over interval 2
      for(i = pos2init; i < pos2end; i++)
      {
        for(j = 0; j < 3; j++)
        {
          org_vec2[j] += org_B[j + 3*i];
        }
      }

      // 4/4: Division by total number of points
      for(i = 0; i < 3; i++)
      {
        org_vec1[i] /= (pos1end - pos1init);
        org_vec2[i] /= (pos2end - pos2init);
      }

      // The hybrid MVA procedure
      // See Mistry+ GRL 2015 for details

      //   Obtain the direction normal to the current sheet
      crossprod(org_vec1, org_vec2, Ndir);
      // Normalize vector
      temp_modB = mag(Ndir);
      for(i = 0; i < 3; i++)
      {
        Ndir[i] = Ndir[i]/temp_modB;
      }

      //   Obtain the direction 'M' to the current sheet
      crossprod(Ndir, mva_vec, Mdir);
      //   Normalize vector
      temp_modB = mag(Mdir);
      for(i = 0; i < 3; i++)
      {
        Mdir[i] = Mdir[i]/temp_modB;
      }

      //   Obtain the direction 'L' to the current sheet
      crossprod(Mdir, Ndir, Ldir);
      //   Normalize vector
      temp_modB = mag(Ldir);
      for(i = 0; i < 3; i++)
      {
        Ldir[i] = Ldir[i]/temp_modB;
      }

      // Let us write the result merrily!
      for(i = 0; i < file_length; i++)
      {
        // Use org_vec1 as a temporary variable
        for(j = 0; j < 3; j++)
        {
          org_vec1[j] = org_B[j + 3*i];
        }
        fprintf(fp3, "%lf %lf %lf\n", dotprod(org_vec1, Ldir),
                dotprod(org_vec1, Mdir),
                dotprod(org_vec1, Ndir));
      }



      fclose(fp1);
      fclose(fp2);
      fclose(fp3);

      free(org_B);
      free(hmva_B);
      org_B  = NULL;
      hmva_B = NULL;
    }

  return 0;
}
