c       struct.f
c       A simple code to compute the structure functions
c       Output will be written at a subdir called 'output_structs'
c
c       Compile this file with
c       $ gfortran struct.f -o struct.exe -O3
c
c       Author: Rodrigo Miranda
c
ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

        implicit none
        integer N
        parameter(N=108001)
        integer i, tau, T
        integer file_id
        double precision x(N)
        double precision L(N), L_init, L_fin
        double precision L_3(N)
        double precision q, zeta
        character temp(30)

        open(9, file='time_series.dat', status='old')
        open(10, file='output_structs/output_struct_tau_q1.dat',
     *       status='unknown')
        open(12, file='output_structs/output_struct_tau_q1.25.dat',
     *       status='unknown')
        open(15, file='output_structs/output_struct_tau_q1.5.dat',
     *       status='unknown')
        open(17, file='output_structs/output_struct_tau_q1.75.dat',
     *       status='unknown')
        open(20, file='output_structs/output_struct_tau_q2.dat',
     *       status='unknown')
        open(22, file='output_structs/output_struct_tau_q2.25.dat',
     *       status='unknown')
        open(25, file='output_structs/output_struct_tau_q2.5.dat',
     *       status='unknown')
        open(27, file='output_structs/output_struct_tau_q2.75.dat',
     *       status='unknown')
        open(30, file='output_structs/output_struct_tau_q3.dat',
     *       status='unknown')
        open(32, file='output_structs/output_struct_tau_q3.25.dat',
     *       status='unknown')
        open(35, file='output_structs/output_struct_tau_q3.5.dat',
     *       status='unknown')
        open(37, file='output_structs/output_struct_tau_q3.75.dat',
     *       status='unknown')
        open(40, file='output_structs/output_struct_tau_q4.dat',
     *       status='unknown')
        open(42, file='output_structs/output_struct_tau_q4.25.dat',
     *       status='unknown')
        open(45, file='output_structs/output_struct_tau_q4.5.dat',
     *       status='unknown')
        open(47, file='output_structs/output_struct_tau_q4.75.dat',
     *       status='unknown')
        open(50, file='output_structs/output_struct_tau_q5.dat',
     *       status='unknown')
        open(52, file='output_structs/output_struct_tau_q5.25.dat',
     *       status='unknown')
        open(55, file='output_structs/output_struct_tau_q5.5.dat',
     *       status='unknown')
        open(57, file='output_structs/output_struct_tau_q5.75.dat',
     *       status='unknown')
        open(60, file='output_structs/output_struct_tau_q6.dat',
     *       status='unknown')

        open(110, file='output_structs/unnormalized_q1.dat',
     *       status='unknown')
        open(112, file='output_structs/unnormalized_q1.25.dat',
     *       status='unknown')
        open(115, file='output_structs/unnormalized_q1.5.dat',
     *       status='unknown')
        open(117, file='output_structs/unnormalized_q1.75.dat',
     *       status='unknown')
        open(120, file='output_structs/unnormalized_q2.dat',
     *       status='unknown')
        open(122, file='output_structs/unnormalized_q2.25.dat',
     *       status='unknown')
        open(125, file='output_structs/unnormalized_q2.5.dat',
     *       status='unknown')
        open(127, file='output_structs/unnormalized_q2.75.dat',
     *       status='unknown')
        open(130, file='output_structs/unnormalized_q3.dat',
     *       status='unknown')
        open(132, file='output_structs/unnormalized_q3.25.dat',
     *       status='unknown')
        open(135, file='output_structs/unnormalized_q3.5.dat',
     *       status='unknown')
        open(137, file='output_structs/unnormalized_q3.75.dat',
     *       status='unknown')
        open(140, file='output_structs/unnormalized_q4.dat',
     *       status='unknown')
        open(142, file='output_structs/unnormalized_q4.25.dat',
     *       status='unknown')
        open(145, file='output_structs/unnormalized_q4.5.dat',
     *       status='unknown')
        open(147, file='output_structs/unnormalized_q4.75.dat',
     *       status='unknown')
        open(150, file='output_structs/unnormalized_q5.dat',
     *       status='unknown')
        open(152, file='output_structs/unnormalized_q5.25.dat',
     *       status='unknown')
        open(155, file='output_structs/unnormalized_q5.5.dat',
     *       status='unknown')
        open(157, file='output_structs/unnormalized_q5.75.dat',
     *       status='unknown')
        open(160, file='output_structs/unnormalized_q6.dat',
     *       status='unknown')


        do i = 1, N
c          read(9, 267) temp, x(i)
          read(9, *) x(i)
c          write(*, *) temp, x(i)
        enddo


c       First we calculate L(3, tau) (x axis)
        q = 3
        do tau = 1, N/2
          L_3(tau) = 0.
          do i = 1, N - tau
            L_3(tau) = L_3(tau) + dabs((x(i + tau) - x(i)))**q
          enddo
          L_3(tau) = L_3(tau)/(N - tau)
        enddo

c       Choose T = 1 (Used in the expression L(3, T))
        T = 1

        write(*, *) 'Doing normalization using L(3, T) = ', L_3(T)

        file_id = 10

c       Then we calculate the rest of the structure functions
c        do q = 1, 6, 0.25
        q = 1
        do while (q.le.6)

          write(*, *) 'q = ', q

          L_init = 0.
          L_fin  = 0.

          do tau = 1, N/2
            L(tau) = 0.
            do i = 1, N-tau
              L(tau) = L(tau) + dabs((x(i + tau) - x(i)))**q
            enddo
            L(tau) = L(tau)/(N - tau)
            file_id = int(q*10.)
            write(file_id, *) L_3(tau)/L_3(T), L(tau)/L(T)
            write(file_id+100, *) tau, L(tau)
          enddo

          file_id = file_id + 10

          q = q + 1.0
        enddo

        close(9)
c        do q = 1, 6, 0.25
        q = 1
        do while (q.lt.6)
          file_id = int(q*10)
          close(file_id)
          q = q + 0.25
        enddo

267     format(26A, F12.8)

        end
