function [pe histogr] = pec_dd(y,m,t)

%  Calculate the permutation entropy
%  Resolve ties using the data-driven imputation method
%  from Traversaro et al., CHAOS 2018

%  Input:   y: time series;
%           m: order of permuation entropy
%           t: delay time of permuation entropy, 

% Output: 
%           pe:    permuation entropy
%           hist:  the histogram for the order distribution

%Ref: G Ouyang, J Li, X Liu, X Li, Dynamic Characteristics of Absence EEG Recordings with Multiscale Permutation %     %                             Entropy Analysis, Epilepsy Research, doi: 10.1016/j.eplepsyres.2012.11.003
%     X Li, G Ouyang, D Richards, Predictability analysis of absence seizures with permutation entropy, Epilepsy %     %                            Research,  Vol. 77pp. 70-74, 2007



ly = length(y);
permlist = perms(1:m);
c(1:length(permlist))=0;
ctilde(1:length(permlist))=0;
n_unique = 0;

%%%%%%%%%%%%%%%%
%  Complete case
    
 for j=1:ly-t*(m-1)
     win = y(j:t:j+t*(m-1));
     % No ties
     if(length(win) == length(unique(win)))
       n_unique = n_unique + 1;
       [a,iv]=sort(win);
       for jj=1:length(permlist)
           if (norm(permlist(jj,:)-iv)==0)
               ctilde(jj) = ctilde(jj) + 1 ;
           end
       end
     end
 end

%fprintf('Number of repeated values: %d\n', ly-t*(m-1) - n_unique);

%fprintf 'Complete case: \n'

n_forbid = sum(ctilde == 0);

%printf('  Forbidden states = %d\n', n_forbid);

ptilde = ctilde(find(ctilde~=0))/sum(ctilde(find(ctilde~=0)));
% Unnormalized Shannon entropy
petilde = -sum(ptilde .* log(ptilde));
H_complete = petilde/log(factorial(m));

%printf('  H = %11.9f\n', H_complete);

n_forbid = 0;
ptilde = ctilde/sum(ctilde(find(ctilde~=0)));

%%%%%%%%%%%%%%%%
%  Time-ordered case

ctimeordered(1:length(permlist))=0;

 for j=1:ly-t*(m-1)
     win = y(j:t:j+t*(m-1));
     [a,iv]=sort(win);
     for jj=1:length(permlist)
       if (abs(permlist(jj,:)-iv))==0
         ctimeordered(jj) = ctimeordered(jj) + 1;
       end
     end
 end


%ctilde = ctilde/(length(y) - (m - 1)*t);
ctimeordered = ctimeordered/n_unique;

%fprintf 'Time-ordered case: \n'

n_forbid = sum(ctimeordered == 0);

%printf('  Forbidden states = %d\n', n_forbid);

ptimeordered = ctimeordered(find(ctimeordered~=0))/sum(ctimeordered(find(ctimeordered~=0)));
% Unnormalized Shannon entropy
ptimeordered = -sum(ptimeordered.* log(ptimeordered));
H_to = ptimeordered/log(factorial(m));

%printf('  H = %11.9f\n', H_to);

n_forbid = 0;

count = 0;

c2(1:length(permlist))=0;

 for j=1:ly-t*(m-1)
     win = y(j:t:j+t*(m-1));
     % No ties
     if(length(win) == length(unique(win)))
%       [a,iv]=sort(y(j:t:j+t*(m-1)));
       [a,iv]=sort(win);
       for jj=1:length(permlist)
           if (abs(permlist(jj,:)-iv))==0
               c(jj) = c(jj) + 1 ;
               count = count + 1;
           end
       end
     else
       % Break ties adding random noise
       % at positions of repeated values.
       % Repeated positions are indicated by a '1'
       % and '0' otherwise.
       % Ex.: win = [3 7 3] -> mask = [1 0 1]
       if (length(unique(win)) > 1)
         [NN XX] = hist(win, unique(win));
         peak = hist(win, unique(win)) > 1;
%         [NN XX] = hist(win, unique(win));
%         peak = hist(win, unique(win)) > 1;
%         mask = ismember(win, nonzeros(XX.*peak));
         mask = ismember(win, (XX(peak)));
       else
         mask = ones(1, m);
       end
       % Add random noise at positions
       % indicated by 'mask'
       winr = win + 1.e-1*rand(1, m).*mask;

       % Compute weight
       % Generate new array of possible permutations

       if (length(unique(win)) > 1)
         [NN XX] = hist(win, unique(win));

         repeated = hist(win, unique(win)) > 1;

         tempn = 1;
         for i=1:length(repeated)
           tempn = tempn*factorial(NN(i));
         end

         [a2, iv2] = sort(win);

         possibleperms = zeros(tempn, length(win));
         prevmat = [];

         repeatedelems = XX(repeated);
         NNrepeated = NN(repeated);

         maskmat = zeros(tempn, length(win));

         for i=1:sum(repeated)
           mask = i*ismember(a2, repeatedelems(i));
%  maskmat = maskmat + ones(factorial(length(find(mask==1))), 1)*mask
           maskmat = maskmat + ones(tempn, 1)*mask;
         end

         for i=1:sum(repeated)
           mask = ismember(a2, repeatedelems(i));
           iv2mask = iv2(mask);
%  maskmat = ones(factorial(length(find(mask==1))), 1)*mask
           submattmp = iv2mask(perms(1:NNrepeated(i)));
           tempmat = [];
           if(size(prevmat, 1) ~= 0)
             for j=1:size(prevmat, 1)
               for k=1:size(submattmp, 1)
                 tempmat = [tempmat; prevmat(j, :) submattmp(k, :)];
               end
             end
           else
             tempmat = submattmp;
           end
           prevmat = tempmat;
         end

maskmat;
prevmat;

         possibleperms(find(maskmat > 0)) = prevmat;

         if(sum(sum(maskmat == 0)) > 0)
%  win
%  iv2
%  (maskmat == 0)
%  (maskmat == 0).*iv2
%%  possibleperms(find(maskmat == 0)) = nonzeros((maskmat == 0).*iv2);
           possibleperms(find(maskmat == 0)) = ones(size(possibleperms, 1), 1)*iv2(maskmat(1, :) == 0);
         end

       else
         possibleperms = perms(1:m);
       end

       % Compute the sum of the probability of all suitable patterns
       % using the probabilities from the complete case
       sumctilde = 0.;
       for jj=1:length(permlist)
         for kk=1:size(possibleperms, 1)
           if (norm(permlist(jj,:)-possibleperms(kk, :))==0)
               sumctilde = sumctilde + ctilde(jj);
           end
         end
       end

         for kk=1:size(possibleperms, 1)
            for jj=1:length(permlist)
             if (norm(permlist(jj,:)-possibleperms(kk, :))==0)
               count = count + 1;
               if(sumctilde != 0)
                 c(jj) = c(jj) + ctilde(jj)/sumctilde;
               else
%                 fprintf('    Data-driven imputation: FMP detected.\n');
                 c(jj) = c(jj) + 1/size(possibleperms, 1);
               end
             end
           end
         end

     end
 end

%printf('Data-driven case: \n');
n_forbid = sum(c == 0);
%printf('  Forbidden states = %d\n', n_forbid);

c = c(find(c~=0))/sum(c(find(c~=0)));

histogr = c;
 
sum(c);
p = c;
% Unnormalized Shannon entropy
pe = -sum(p .* log(p));

%H_dd = pe/log(factorial(m));
%printf('  H = %11.9f\n', H_dd);
