NW=100
NDATA=`wc -l ../exhaust.dat | awk '{print $1}'`
NWINDOW=`echo $NDATA - $NW | bc`
STEP=5

echo $NW $NDATA $NWINDOW

################
echo BL

for i in `seq 0 $STEP $NW`; do

  numlines=`echo $NDATA - $NW + $i | bc`

  echo "  " $numlines

#  cat ../exhaust.dat | awk '{print $1}' > time_series.dat 
  head -n $numlines ../exhaust.dat | tail -n $NWINDOW | awk '{print $1}' > time_series.dat 
  octave -q jsdd.m >> Cjs_BL.dat

done

################
echo BM

for i in `seq 0 $STEP $NW`; do

  numlines=`echo $NDATA - $NW + $i | bc`

  echo "  " $numlines

#  cat ../exhaust.dat | awk '{print $2}' > time_series.dat 
  head -n $numlines ../exhaust.dat | tail -n $NWINDOW | awk '{print $2}' > time_series.dat 
  octave -q jsdd.m >> Cjs_BM.dat

done

################
echo BN

for i in `seq 0 $STEP $NW`; do

  numlines=`echo $NDATA - $NW + $i | bc`

  echo "  " $numlines

#  cat ../exhaust.dat | awk '{print $3}' > time_series.dat 
  head -n $numlines ../exhaust.dat | tail -n $NWINDOW | awk '{print $3}' > time_series.dat 
  octave -q jsdd.m >> Cjs_BN.dat
done
