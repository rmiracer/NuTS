d = 3;
N = factorial(d);

ps = ones(60, N);
ps(:, 1) = linspace(1/N, 1, 60);

for i=2:N,
  ps(:, i) = (1 - ps(:, 1))/(N - 1);
endfor

pe = (1/N)*ones(60, N);
ppluspe = (ps + pe)/2;

a1 = -sum(ppluspe.*log(ppluspe), 2);
a2 = -sum(ps.*log(ps), 2);
a3 = -sum(pe.*log(pe), 2);

cmin = -2*(a1 - 0.5*a2 - 0.5*a3)/( ((N+1)/N)*log(N+1) - 2*log(2*N) + log(N));

cmin(end) = 1;

Hmin = -sum(ps.*log(ps), 2)/log(N);
%H = linspace(1, 0, 60)';

Hmin(end) = 0;

%plot(H, cmin.*H)
%axis([0 1 0 0.80]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%cmaxbuffer = zeros(119, 60);
cmaxbuffer = zeros(119*60, 1);
Hbuffer = zeros(119*60, 1);

for i = (N - 1):-1:1
%  i = N - 5;

  ps1 = zeros(60, N);
  ps1_nozeros = zeros(60, N - i + 1);

  ps1(:, 1)         = linspace(0, 1/(N - i + 1), 60);
  ps1_nozeros(:, 1) = linspace(0, 1/(N - i + 1), 60);

  for j=2:(N - i + 1)
    ps1(:, j)         = (1 - ps1(:, 1))/(N - i);
    ps1_nozeros(:, j) = (1 - ps1(:, 1))/(N - i);
  endfor

  pe = (1/(N))*ones(60, N);
  ppluspe = (ps1 + pe)/2;

  a1 = -sum(ppluspe.*log(ppluspe), 2);
  a2 = -sum(ps1_nozeros.*log(ps1_nozeros), 2);
  a3 = -sum(pe.*log(pe), 2);

  cmax = -2*(a1 - 0.5*a2 - 0.5*a3)/( ((N+1)/N)*log(N+1) - 2*log(2*N) + log(N));

%  cmax(end) = 1;

  H = -sum(ps1_nozeros.*log(ps1_nozeros), 2)/log(N);
%H = linspace(1, 0, 60)';

%  H(end) = 0;

%  cmaxbuffer(i, :) = cmax.*H;
  cmaxbuffer( ((i - 1)*60 + 1):i*60) = cmax;
  Hbuffer( ((i - 1)*60 + 1):i*60) = H;

endfor

plot(Hmin, cmin.*Hmin, 'b', Hbuffer, cmaxbuffer.*Hbuffer, 'b');

%hold on
%for i = 1:(N - 1)
%  plot(H, cmaxbuffer(i, :)'.*H)
%endfor

%  plot(H, cmax.*H)
%  axis([0 1 0 0.80]);

